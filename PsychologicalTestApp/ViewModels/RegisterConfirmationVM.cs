﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using PsychologicalTestApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.ViewModels
{
    public class RegisterConfirmationVM
    {
        //private readonly UserManager<User> _userManager;
        //private readonly IEmailSender _sender;

        //public RegisterConfirmationModel(UserManager<User> userManager, IEmailSender sender)
        //{
        //    _userManager = userManager;
        //    _sender = sender;
        //}
        public string Email { get; set; }

        public bool DisplayConfirmAccountLink { get; set; }

        public string EmailConfirmationUrl { get; set; }
    }
}
