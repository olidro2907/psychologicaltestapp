﻿using PsychologicalTestApp.Data;
using PsychologicalTestApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.ViewModels
{
    public class ParticipantListIndexVM
    {
        private readonly AppDbContext _context;
        public ParticipantListIndexVM(AppDbContext context)
        {
            _context = context;
        }

        public int CurrentPage { get; set; } = 1;

        public int Count { get; set; }

        public int PageSize { get; set; } = 10;

        public int TotalPages => (int)Math.Ceiling(decimal.Divide(Count, PageSize));

        public bool EnablePrevious => CurrentPage > 1;

        public bool EnableNext => CurrentPage < TotalPages;

        public IList<Participant> Participants { get; set; } = new List<Participant>();

        public void OnGet(int currentPage)
        {
            CurrentPage = currentPage == 0 ? 1 : currentPage;

            Count = _context.Participants.Count();

            if (CurrentPage > TotalPages)
                CurrentPage = TotalPages;

            Participants = _context.Participants
                .Skip((CurrentPage - 1) * PageSize)
                .Take(PageSize)
                .ToList();
        }

    }
}
