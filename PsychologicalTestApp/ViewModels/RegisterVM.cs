﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.ViewModels
{
    public class RegisterVM
    {
        //[Required(ErrorMessage = "Proszę podaj nazwę użytkownika.")]
        //[Display(Name = "Nazwa użytkownika")]
        //public string UserName { get; set; }

        [Required(ErrorMessage = "Proszę podaj adress email.")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Proszę podaj hasło.")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Proszę powtórz hasło.")]
        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        public string ConfirmPassword { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Wymagana zgoda.")]
        public bool Consent { get; set; }

    }
}
