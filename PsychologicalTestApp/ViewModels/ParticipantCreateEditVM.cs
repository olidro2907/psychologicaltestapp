﻿using PsychologicalTestApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.ViewModels
{
    public class ParticipantCreateEditVM: UserCreateEditVM
    {
        public int? Id { get; set; }

        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Range(1, 116)]
        [Display(Name = "Wiek")]
        public int? Age { get; set; }

        [Required(ErrorMessage ="Płeć jest wymagana")]
        [Display(Name = "Płeć")]
        public Gender? Gender { get; set; }

        [Display(Name = "Zedytowany")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime UpdateDate { get; set; }

        //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        //[Display(Name = "Data urodzenia")]
        //public DateTime Date_of_birth { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }
    }
}
