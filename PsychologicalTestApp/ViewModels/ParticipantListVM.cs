﻿using PsychologicalTestApp.Models;
using PsychologicalTestApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PsychologicalTestApp.ViewModels
{
    public class ParticipantListVM
    {
        public string Title { get; set; }
        public IEnumerable<ParticipantListItemVM> Participants { get; set; }

        public ParticipantSortEnum SortType { get; set; }

        public IEnumerable<ParticipantListItemVM> Sort(IEnumerable<ParticipantListItemVM> listElements)
        {
            switch (SortType)
            {
                case ParticipantSortEnum.FirstNameAsc:
                    return listElements.OrderBy(c => c.FirstName);
                case ParticipantSortEnum.FirstNameDesc:
                    return listElements.OrderByDescending(c => c.FirstName);
                case ParticipantSortEnum.LastNameAsc:
                    return listElements.OrderBy(c => c.LastName);
                case ParticipantSortEnum.LastNameDesc:
                    return listElements.OrderByDescending(c => c.LastName);
                case ParticipantSortEnum.AgeAsc:
                    return listElements.OrderBy(c => c.Age);
                case ParticipantSortEnum.AgeDesc:
                    return listElements.OrderByDescending(c => c.Age);
                case ParticipantSortEnum.CreateDateAsc:
                    return listElements.OrderBy(c => c.CreateDate);
                case ParticipantSortEnum.CreateDateDesc:
                    return listElements.OrderByDescending(c => c.CreateDate);
                default:
                    throw new ArgumentException();
            }
        }
    }
}
