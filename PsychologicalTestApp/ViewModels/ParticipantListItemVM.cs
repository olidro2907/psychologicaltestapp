﻿using PsychologicalTestApp.Models;
using PsychologicalTestApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.ViewModels
{
    public class ParticipantListItemVM
    {
        public ParticipantListItemVM() { }

        public ParticipantListItemVM(Participant participant)
        {
            Id = participant.Id;
            FirstName = participant.FirstName;
            LastName = participant.LastName;
            Age = participant.Age;
            Gender = participant.Gender;
            Email = participant.Email;
            UserId = participant.UserId;
            Description = participant.Description;
            UpdateDate = participant.UpdateDate;
            CreateDate = participant.CreateDate;
        }

        public string Id { get; set; }

        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Range(1, 116)]
        [Display(Name = "Wiek")]
        public int Age { get; set; }

        [Display(Name = "Płeć")]
        public Gender? Gender { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //[Phone]
        //[Display(Name = "Numer telefonu")]
        //public string PhoneNumber { get; set; }

        public string UserId{ get; set; }

        [Display(Name = "Utworzony")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Zedytowany")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "Potwierdzony")]
        public bool Confirm { get; set; }
    }
}
