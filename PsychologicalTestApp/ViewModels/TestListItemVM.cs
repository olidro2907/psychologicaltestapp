﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PsychologicalTestApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.ViewModels
{
    public class TestListItemVM
    {
        public TestListItemVM(Test test, IEnumerable<SelectListItem> testCategories)
        {
            TestId = test.TestId;
            Name = test.Name;
            Description = test.Description;
            AddDate = test.AddDate;

            var testCategoriesList = testCategories.Where(x => x.Value != test.CategoryId.ToString()).ToList();
        }

        public string TestId { get; set; }

        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        [Display(Name = "Kategoria")]
        public TestCategory Category { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Data utworzenia")]

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime AddDate { get; internal set; }
    }
}
