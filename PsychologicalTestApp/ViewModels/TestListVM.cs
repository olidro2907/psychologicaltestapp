﻿using PsychologicalTestApp.Data;
using PsychologicalTestApp.Models;
using PsychologicalTestApp.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.ViewModels
{
    public class TestListVM
    {
        private AppDbContext _context;

        public TestListVM(){}

        public TestListVM(AppDbContext context)
        {
            _context = context;
        }

        public string Title { get; set; }
        public IEnumerable<Test> Tests { get; set; }
        public TestSortEnum SortType { get; set; }

        public IEnumerable<TestListItemVM> Sort(IEnumerable<TestListItemVM> listElements)
        {
            switch (SortType)
            {
                case TestSortEnum.NameAsc:
                    return listElements.OrderBy(c => c.Name);
                case TestSortEnum.NameDesc:
                    return listElements.OrderByDescending(c => c.Name);
                case TestSortEnum.CategoryAsc:
                    return listElements.OrderBy(c => c.Category.Name);
                case TestSortEnum.CategoryDesc:
                    return listElements.OrderByDescending(c => c.Category.Name);
                case TestSortEnum.AddDateAsc:
                    return listElements.OrderBy(c => c.AddDate);
                case TestSortEnum.AddDateDesc:
                    return listElements.OrderByDescending(c => c.AddDate);
                default:
                    throw new ArgumentException();
            }
        }
    }
}
