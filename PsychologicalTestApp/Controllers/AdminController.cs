﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PsychologicalTestApp.Data;
using PsychologicalTestApp.Models;
using PsychologicalTestApp.ViewModels;

namespace PsychologicalTestApp.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly AppDbContext _db;
        private IUserValidator<User> _userValidator;
        private IPasswordValidator<User> _passwordValidator;
        private IPasswordHasher<User> _passwordHasher;
        public AdminController(UserManager<User> userManager, AppDbContext db, IUserValidator<User> userValidator, IPasswordValidator<User> passwordValidator, IPasswordHasher<User> passwordHasher)
        {
            _userManager = userManager;
            _db = db;
            _userValidator = userValidator;
            _passwordValidator = passwordValidator;
            _passwordHasher = passwordHasher;
        }
    
        [HttpGet]
        public IActionResult Index()
        {
            var model = new ParticipantListIndexVM(_db);
            model.PageSize = 10;
            model.OnGet(1);

            return View(model);
        }

        [HttpGet]
        public IActionResult CreateParticipant()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreateTest()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateParticipant(ParticipantCreateEditVM participantVM)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(participantVM.Email);
                if (user == null)
                {
                    user = new User()
                    {
                        UserName = participantVM.UserName,
                        Email = participantVM.Email,
                        CreateDate = DateTime.Now,
                        EmailConfirmed = true
                    };
                    if (participantVM.Password == participantVM.ConfirmPassword)
                    {
                        var result = await _userManager.CreateAsync(user, participantVM.Password);

                        if (result.Succeeded)
                        {
                            await _userManager.AddToRoleAsync(user, "Participant");
                            await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, "Participant"));
                            Participant participant = new Participant
                            {
                                CreateDate = user.CreateDate,
                                Email = user.Email,
                                Confirm = true,
                                UserId = user.Id,
                                User = user
                            };
                            _db.Participants.Add(participant);
                            _db.SaveChanges();


                            return RedirectToAction("Participants");

                        }
                        else
                        {
                            var list_of_errors = result.Errors.ToList();
                            foreach (var error in list_of_errors)
                            {
                                if (error.Code.Contains("Password"))
                                {
                                    ModelState.AddModelError("password", error.Description);
                                }
                                else if (error.Code.Contains("User"))
                                {
                                    ModelState.AddModelError("username", error.Description);
                                }
                                else if (error.Code.Contains("Email"))
                                {
                                    ModelState.AddModelError("email", error.Description);
                                }
                            }
                        }
                    }
                    else
                        ModelState.AddModelError("password", "Podane hasło i powtórzone hasło musi się zgadzać.");
                }
                else
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
            }
            return View(participantVM);
        }

        [HttpGet]
        public async Task<IActionResult> Update(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                UserCreateEditVM userVM = new UserCreateEditVM { UserName = user.UserName, Email = user.Email, UserId = user.Id };
                return View(userVM);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public async Task<IActionResult> UpdateParticipant(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var participant = _db.Participants.FirstOrDefault(p => p.UserId == user.Id);
                if (participant == null)
                {
                    return NotFound();
                }
                ParticipantCreateEditVM participantVM = new ParticipantCreateEditVM { UserName = user.UserName, Email = participant.Email, FirstName = participant.FirstName, LastName = participant.LastName, Age = participant.Age, Gender = participant.Gender, Description = participant.Description, UserId = user.Id };

                return View(participantVM);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(UserCreateEditVM userVM)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(userVM.UserId);
                bool change_userName = false;
                bool change_email = false;
                bool change_password = false;

                if (user != null)
                {
                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(userVM.UserName) && userVM.UserName != user.UserName)
                    {
                        user.UserName = userVM.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(userVM);
                        }
                        change_userName = true;
                    }                   

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(userVM.Email) && userVM.Email != user.Email)
                    {
                        user.Email = userVM.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {
                            AddErrorsFromResult(validEmail);
                            return View(userVM);
                        }
                        change_email = true;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(userVM.Password)) && (!string.IsNullOrEmpty(userVM.ConfirmPassword)) && (!string.IsNullOrEmpty(userVM.OldPassword)) && (userVM.OldPassword != userVM.Password) && (userVM.Password == userVM.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, userVM.Password);
                        if (validPass.Succeeded)
                        {
                            var result = _userManager.ChangePasswordAsync(user, userVM.OldPassword, userVM.Password);
                            if (result.Result.Succeeded)
                            {
                                user.PasswordHash = _passwordHasher.HashPassword(user, userVM.Password);
                                change_password = true;
                            }
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                            return View(userVM);
                        }
                    }
                    else if ((!string.IsNullOrEmpty(userVM.OldPassword)) && (!string.IsNullOrEmpty(userVM.Password)) && (userVM.OldPassword == userVM.Password))
                    {
                        ModelState.AddModelError("oldpassword", "Stare hasło i nowe hasło musi być różne.");
                        return View(userVM);
                    }
                    else if (userVM.Password != userVM.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(userVM);
                    }


                    if (change_userName || change_email || change_password)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nie znaleziono użytkownika o takiej nazwie.");
                }
            }

            return View(userVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateParticipant(ParticipantCreateEditVM model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.UserId);
                bool change_userName = false;
                bool change_firstName = false;
                bool change_lastName = false;
                bool change_description = false;
                bool change_email = false;
                bool change_password = false;
                bool change_age = false;
                bool change_gender = false;

                if (user != null)
                {
                    var participant = _db.Participants.FirstOrDefault(p => p.UserId == user.Id);

                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);

                    if (!string.IsNullOrEmpty(model.UserName) && model.UserName != user.UserName)
                    {
                        var old_userName = user.UserName;
                        user.UserName = model.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(model);
                        }
                       
                        change_userName = true;
                    }

                    if (!string.IsNullOrEmpty(model.FirstName.Trim()) && model.FirstName.Trim() != participant.FirstName.Trim())
                    {
                        participant.FirstName = model.FirstName.Trim();
                        change_firstName = true;
                    }

                    if (!string.IsNullOrEmpty(model.LastName.Trim()) && model.LastName != participant.LastName.Trim())
                    {
                        participant.LastName = model.LastName.Trim();
                        change_lastName = true;
                    }

                    if (!string.IsNullOrEmpty(model.Description) && model.Description != participant.Description)
                    {
                        participant.Description = model.Description;
                        change_description = true;
                    }

                    if (model.Age != participant.Age)
                    {
                        if (model.Age <= 116 && model.Age >= 1)
                        {
                            participant.Age = model.Age.GetValueOrDefault();
                            change_age = true;
                        }
                        else
                        {
                            ModelState.AddModelError("age", "Niepoprawny wiek.");
                            return View(model);
                        }
                    }

                    if (model.Gender != participant.Gender)
                    {
                        if (!string.IsNullOrEmpty(model.Gender.ToString()))
                        {
                            participant.Gender = model.Gender;
                            change_gender = true;
                        }
                        else
                        {
                            ModelState.AddModelError("Gender", "Płeć jest wymagana.");
                            return View(model);
                        }
                    }

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(model.Email) && model.Email != user.Email)
                    {
                        user.Email = model.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {
                            AddErrorsFromResult(validEmail);
                            return View(model);
                        }
                        change_email = true;
                        participant.Email = model.Email;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(model.Password)) && (model.Password == model.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, model.Password);
                        if (validPass.Succeeded)
                        {
                            user.PasswordHash = _passwordHasher.HashPassword(user, model.Password);
                            change_password = true;
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                        }
                    }
                    else if (model.Password != model.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(model);
                    }

                    if (change_userName || change_firstName || change_lastName || change_email || change_password || change_age || change_description || change_gender)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            participant.UpdateDate = DateTime.Now;
                            _db.Update(participant);
                            _db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);

            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                     return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Nie znaleziono użytkownika.");
                    //throw new NullReferenceException(nameof(userId));
                }
            }
            return View(user);
        }

        public IActionResult Participants()
        {
            //model.SaveSession(Session);

            var newModel = new ParticipantListVM()
            {
                Participants = _db.Participants.Select(x => new ParticipantListItemVM(x)).ToList().OrderBy(c => c.Email)
            };

            return PartialView("_Participants", newModel);
        }

        [HttpGet]
        public IActionResult Tests()
        {
            var testListVM = new TestListVM()
            {
                Tests = _db.Tests.OrderByDescending(o => o.AddDate).ToList()
            };
            return View(testListVM);
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
            {
                if (error.Code.Contains("Password"))
                {
                    ModelState.AddModelError("password", error.Description);
                }
                else if (error.Code.Contains("User"))
                {
                    ModelState.AddModelError("username", error.Description);
                }
                else if (error.Code.Contains("Email"))
                {
                    ModelState.AddModelError("email", error.Description);
                }
                else
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
        }
    }
}
