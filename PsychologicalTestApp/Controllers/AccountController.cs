﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using PsychologicalTestApp.Data;
using PsychologicalTestApp.Models;
using PsychologicalTestApp.ViewModels;

namespace PsychologicalTestApp.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger<User> _logger;
        private readonly AppDbContext _db;
        private readonly IEmailSender _sender;

        public AccountController(SignInManager<User> signInManager, UserManager<User> userManager, RoleManager<IdentityRole> roleManager, ILogger<User> logger, AppDbContext db, IEmailSender sender)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _logger = logger;
            _db = db;
            _sender = sender;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Log
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginVM model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    if (!_userManager.IsEmailConfirmedAsync(user).Result)
                    {
                        ModelState.AddModelError("", "Email nie został potwierdzony!");
                        return View(model);
                    }

                    var participant = _db.Participants.FirstOrDefault(p => p.UserId == user.Id);
                    if (participant != null && !participant.Confirm)
                    {
                        participant.Confirm = true;
                        _db.SaveChanges();
                    }

                    await _signInManager.SignOutAsync();
                    var result = await _signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, false);

                    if (result.Succeeded)
                    {
                        if (await _userManager.IsInRoleAsync(user, "Admin") || participant.Confirm)
                        {
                            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                            {
                                return Redirect(returnUrl ?? "/");
                            }
                            else
                            {
                                if (await _userManager.IsInRoleAsync(user, "Admin"))
                                {
                                    return RedirectToAction("Index", "Admin");
                                }
                                else
                                    return RedirectToAction("Index", "Participant");
                            }
                        }
                        else
                            ModelState.AddModelError("", "Konto użytkownika musi zostać potwierdzone.");
                    }
                    else
                        ModelState.AddModelError("", "Email lub hasło jest nieprawidłowe.");
                }
                else
                    ModelState.AddModelError("", "Email lub hasło jest nieprawidłowe.");
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
        #endregion

        #region Register
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View(new RegisterVM());
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult EmailNotFound()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult NotChangePassword()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userid, string token)
        {
            var user = await _userManager.FindByIdAsync(userid);
            string msg;

            if (user == null)
            {
                ViewBag.msg = "NotFound";
                return View();
            }
            if (user.EmailConfirmed)
            {
                ViewBag.msg = "EmailConfirmed";
                return View();
            }            

            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                var participant = _db.Participants.FirstOrDefault(p => p.UserId == userid);
                participant.Confirm = true;
                _db.SaveChanges();
            }

            return View(result.Succeeded ? nameof(ConfirmEmail) : ViewBag.msg = "NotConfirmed");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(string userid, string code)
        {
            var user = await _userManager.FindByIdAsync(userid);

            if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
            {
                return RedirectToAction("EmailNotFound");
            }

            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordVM model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.UserId);

                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    return RedirectToAction("EmailNotFound");
                }

                if (model.Password == model.ConfirmPassword)
                {
                    var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
                    return View(result.Succeeded ? nameof(Login) : "NotChangePassword");
                }
                else
                    ModelState.AddModelError("password", "Podane hasło i powtórzone hasło muszą się zgadzać.");
            } 
            
            return View();
        }


        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirm()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View(new ForgotPasswordVM());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordVM model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    ViewBag.msg = "NotFound";
                    return RedirectToAction("EmailNotFound");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                await _sender.SendEmailAsync(model.Email, "Zresetuj hasło",
                   $"Proszę zresetuj swoje hasło klikając w link: <a href='{callbackUrl}'>link</a>");
                return RedirectToAction("ForgotPasswordConfirm");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterVM model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    var lettersOfUserName = model.Email.Take(model.Email.IndexOf('@'));
                    string userName = string.Empty;
                    foreach(var item in lettersOfUserName)
                    {
                        userName += item;
                    }
                    if (string.IsNullOrEmpty(userName))
                        throw new ArgumentNullException("User is empty or null");

                    user = new User() { UserName = userName, Email = model.Email, CreateDate = DateTime.Now };
                    if (model.Password == model.ConfirmPassword)
                    {
                        var result = _userManager.CreateAsync(user, model.Password).Result;
                        if (result.Succeeded)
                        {
                            if (!_roleManager.RoleExistsAsync("Participant").Result)
                            {
                                var roleResult = await _roleManager.CreateAsync(new IdentityRole("Participant"));

                                if (!roleResult.Succeeded)
                                {
                                    ModelState.AddModelError("", "Błąd przy utworzeniu roli!");
                                    return View(model);
                                }
                            }

                            await _userManager.AddToRoleAsync(user, "Participant");
                            await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, "Participant"));

                            Participant participant = new Participant
                            {
                                CreateDate = user.CreateDate,
                                Email = user.Email,
                                UserId = user.Id,
                                User = user,
                                Confirm = false
                            };

                            _db.Participants.Add(participant);
                            _db.SaveChanges();

                            string confirmationToken = _userManager.GenerateEmailConfirmationTokenAsync(user).Result;

                            string confirmationLink = Url.Action("ConfirmEmail","Account", new
                            {
                                userid = user.Id,
                                token = confirmationToken
                            },
                               protocol: HttpContext.Request.Scheme);

                            await _sender.SendEmailAsync(model.Email, "Potwierdź swoje konto", $"Proszę potwierdź swoje konto klikając na link: <a href='{confirmationLink}'>link</a>");
                            //await _signInManager.SignInAsync(user, isPersistent: false); automatyczne logowanie nowo utworzonego uzytkownika

                            _logger.LogInformation(3, "User created a new account with password.");                           

                            return RedirectToAction("Index", "Account");
                        }
                        else
                        {
                            var list_of_errors = result.Errors.ToList();
                            foreach (var error in list_of_errors)
                            {
                                if (error.Code.Contains("Password"))
                                {
                                    ModelState.AddModelError("password", error.Description);
                                }
                                else if (error.Code.Contains("Email"))
                                {
                                    ModelState.AddModelError("email", error.Description);
                                }
                                else if (error.Code.Contains("User"))
                                {
                                    ModelState.AddModelError("username", error.Description);
                                }
                                else
                                    ModelState.AddModelError("", error.Description);
                            }
                        }
                    }
                    else
                        ModelState.AddModelError("password", "Podane hasło i powtórzone hasło muszą się zgadzać.");
                }
                else
                    ModelState.AddModelError("email", "Konto z podanym emailem już istnieje");
            }

            return View(model);
        }
        #endregion
    }
}

