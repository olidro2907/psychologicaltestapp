﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using PsychologicalTestApp.Data;
using PsychologicalTestApp.Models;
using PsychologicalTestApp.ViewModels;

namespace PsychologicalTestApp.Controllers
{
    public class ParticipantController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly AppDbContext _db;
        private IUserValidator<User> _userValidator;
        private IPasswordValidator<User> _passwordValidator;
        private readonly IActionContextAccessor _accessor;
        private IPasswordHasher<User> _passwordHasher;

        public ParticipantController(UserManager<User> userManager, AppDbContext db, IUserValidator<User> userValidator, IPasswordValidator<User> passwordValidator, IPasswordHasher<User> passwordHasher, IActionContextAccessor accessor)
        {
            _userManager = userManager;
            _db = db;
            _userValidator = userValidator;
            _passwordValidator = passwordValidator;
            _passwordHasher = passwordHasher;
            _accessor = accessor;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Update(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var participant = _db.Participants.FirstOrDefault(p => p.UserId == user.Id);
                if (participant == null)
                {
                    return NotFound();
                }
                ParticipantCreateEditVM participantVM = new ParticipantCreateEditVM { UserName = user.UserName, Email = participant.Email, FirstName = participant.FirstName, LastName = participant.LastName, Age = participant.Age, Gender = participant.Gender, Description = participant.Description, UserId = user.Id };

                return View(participantVM);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(ParticipantCreateEditVM model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.UserId);
                bool change_userName = false;
                bool change_firstName = false;
                bool change_lastName = false;
                bool change_description = false;
                bool change_email = false;
                bool change_password = false;
                bool change_age = false;
                bool change_gender = false;

                if (user != null)
                {
                    var participant = _db.Participants.FirstOrDefault(p => p.UserId == user.Id);

                    IdentityResult valid = await _userValidator.ValidateAsync(_userManager, user);

                    if (!string.IsNullOrEmpty(model.UserName) && model.UserName != user.UserName)
                    {
                        var old_userName = user.UserName;
                        user.UserName = model.UserName;
                        valid = await _userValidator.ValidateAsync(_userManager, user);
                        if (!valid.Succeeded)
                        {
                            AddErrorsFromResult(valid);
                            return View(model);
                        }

                        change_userName = true;
                    }

                    if (!string.IsNullOrEmpty(model.FirstName.Trim()) && model.FirstName.Trim() != participant.FirstName.Trim())
                    {
                        participant.FirstName = model.FirstName.Trim();
                        change_firstName = true;
                    }

                    if (!string.IsNullOrEmpty(model.LastName.Trim()) && model.LastName != participant.LastName.Trim())
                    {
                        participant.LastName = model.LastName.Trim();
                        change_lastName = true;
                    }

                    if (!string.IsNullOrEmpty(model.Description) && model.Description != participant.Description)
                    {
                        participant.Description = model.Description;
                        change_description = true;
                    }

                    if (model.Age != participant.Age)
                    {
                        if (model.Age <= 116 && model.Age >= 1)
                        {
                            participant.Age = model.Age.GetValueOrDefault();
                            change_age = true;
                        }
                        else
                        {
                            ModelState.AddModelError("age", "Niepoprawny wiek.");
                            return View(model);
                        }
                    }

                    if (model.Gender != participant.Gender)
                    {
                        if (!string.IsNullOrEmpty(model.Gender.ToString()))
                        {
                            participant.Gender = model.Gender;
                            change_gender = true;
                        }
                        else
                        {
                            ModelState.AddModelError("Gender", "Płeć jest wymagana.");
                            return View(model);
                        }
                    }

                    IdentityResult validEmail = await _userValidator.ValidateAsync(_userManager, user);
                    if (!string.IsNullOrEmpty(model.Email) && model.Email != user.Email)
                    {
                        user.Email = model.Email;
                        validEmail = await _userValidator.ValidateAsync(_userManager, user);
                        if (!validEmail.Succeeded)
                        {
                            AddErrorsFromResult(validEmail);
                            return View(model);
                        }
                        change_email = true;
                        participant.Email = model.Email;
                    }

                    IdentityResult validPass = null;
                    if ((!string.IsNullOrEmpty(model.Password)) && (model.Password == model.ConfirmPassword))
                    {
                        validPass = await _passwordValidator.ValidateAsync(_userManager, user, model.Password);
                        if (validPass.Succeeded)
                        {
                            user.PasswordHash = _passwordHasher.HashPassword(user, model.Password);
                            change_password = true;
                        }
                        else
                        {
                            AddErrorsFromResult(validPass);
                        }
                    }
                    else if (model.Password != model.ConfirmPassword)
                    {
                        ModelState.AddModelError("password", "Nowe hasło i powtórzone hasło muszą się zgadzać.");
                        return View(model);
                    }

                    if (change_userName || change_firstName || change_lastName || change_email || change_password || change_age || change_description || change_gender)
                    {
                        IdentityResult result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            participant.UpdateDate = DateTime.Now;
                            _db.Update(participant);
                            _db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            AddErrorsFromResult(result);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Nazwa użytkownika jest już zajęta.");
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Tests()
        {
            var testListVM = new TestListVM()
            {
                Tests = _db.Tests.OrderByDescending(o => o.AddDate).ToList()
            };
            return View(testListVM);
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
            {
                if (error.Code.Contains("Password"))
                {
                    ModelState.AddModelError("password", error.Description);
                }
                else if (error.Code.Contains("User"))
                {
                    ModelState.AddModelError("username", error.Description);
                }
                else if (error.Code.Contains("Email"))
                {
                    ModelState.AddModelError("email", error.Description);
                }
                else
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
        }
    }
}
