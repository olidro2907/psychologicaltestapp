﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PsychologicalTestApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PsychologicalTestApp.Data
{
    public class Seed
    {
        public static async Task CreateUserRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            IdentityResult roleResult;
            string roleName = "Participant";

            if (!await roleManager.RoleExistsAsync(roleName))
            {
                roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
            }

            if (await userManager.FindByEmailAsync("olidro2907@gmail.com") == null)
            {
                var admin = new User()
                {
                    UserName = "admin",
                    Email = "olidro2907@gmail.com",
                    CreateDate = DateTime.Now,
                    EmailConfirmed = true
                };
                await userManager.CreateAsync(admin, "1wsxZAQ2");


                var roleCheck = await roleManager.RoleExistsAsync("Admin");
                IdentityRole adminrole = new IdentityRole();
                adminrole.Name = "Admin";

                if (!roleCheck)
                {
                    roleResult = await roleManager.CreateAsync(adminrole);

                    await roleManager.AddClaimAsync(adminrole, new Claim("Can add roles", "add.role"));
                    await roleManager.AddClaimAsync(adminrole, new Claim("Can delete roles", "delete.role"));
                    await roleManager.AddClaimAsync(adminrole, new Claim("Can edit roles", "edit.role"));
                }

                await userManager.AddToRoleAsync(admin, "Admin");

                if (!roleCheck)
                {
                    await userManager.AddClaimAsync(admin, new Claim(ClaimTypes.Role, "Admin"));
                }
            }
        }

        public static async Task CreateUsers(IServiceProvider serviceProvider, AppDbContext db)
        {
            if (db.Participants.Any())
            {
                return;
            }

            //var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            var listofusers = new List<User>()
                    {
                        new User {
                            UserName = "participant1Test",
                            Email = "participant1Test@mail.com",
                            CreateDate = DateTime.Now,
                            EmailConfirmed = true
                        },
                        new User {
                            UserName = "participant2Test",
                            Email = "participant2Test@mail.com",
                            CreateDate = DateTime.Now,
                            EmailConfirmed = true
                        },
                        new User {
                            UserName = "participantt3Test",
                            Email = "participant3Test@mail.com",
                            CreateDate = DateTime.Now,
                            EmailConfirmed = true
                        },

                        new User {
                            UserName = "participant4Test",
                            Email = "participant4Test@mail.com",
                            CreateDate = DateTime.Now,
                            EmailConfirmed = true
                        },

                        new User {
                            UserName = "participant5Test",
                            Email = "participant5Test@mail.com",
                            CreateDate = DateTime.Now,
                            EmailConfirmed = true
                        },

                        new User {
                            UserName = "participant6Test",
                            Email = "participantt6Test@mail.com",
                            CreateDate = DateTime.Now,
                            EmailConfirmed = true
                        }

                    };

            var i = 0;
            string[] firstnames = new[] { "Adrianna", "Jarosław", "Anna", "Marta", "Lucjan", "Edward"};
            string[] lastnames = new[] { "Szymczak", "Sawicki", "Wiśniewska", "Wilk", "Janicki", "Stankiewicz"};
            int[] ages = new[] {50, 12, 34, 56, 21, 18};
            
            foreach (var user in listofusers)
            {
                var checkUser = await userManager.FindByNameAsync(user.UserName);
                if (checkUser == null)
                {
                    var result = await userManager.CreateAsync(user, user.UserName);
                    if (result.Succeeded)
                    {
                        await userManager.AddToRoleAsync(user, "Participant");
                        await userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, "Participant"));
                        //User copyuser = db.Users.FirstOrDefault(a => a.Id == user.Id);
                        Participant participant = new Participant
                        {
                            CreateDate = user.CreateDate,
                            Email = user.Email,
                            FirstName = firstnames[i],
                            LastName = lastnames[i],
                            PhoneNumber = user.PhoneNumber,
                            Age = ages[i],
                            Confirm = true,
                            UserId = user.Id,
                        };
                        try
                        {
                            db.Participants.Add(participant);
                        }
                        catch(Exception ex) 
                        {
                            throw new Exception(ex.Message);
                        }

                        
                        db.SaveChanges();
                        i++;
                    }
                }
            }
        }

        public static async Task CreateTests(AppDbContext db)
        {
            if (!db.TestCategory.Any())
            {
                TestCategory testCategory1 = new TestCategory
                {
                    Name = Models.Enums.TestType.Adult.ToString(),
                };

                TestCategory testCategory2 = new TestCategory
                {
                    Name = Models.Enums.TestType.Child.ToString(),
                };

                try
                {
                    db.TestCategory.Add(testCategory1);
                    db.TestCategory.Add(testCategory2);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                db.SaveChanges();
            }

            if (!db.Tests.Any())
            {
                Test test1 = new Test
                {
                    Name = "SocialInteligentTest",
                    Category = db.TestCategory.FirstOrDefault(c => c.Name.Contains("Adult")),
                    CategoryId = db.TestCategory.FirstOrDefault(c => c.Name.Contains("Adult")).CategoryId,
                    Description = "Test inteligencji społecznej",
                    AddDate = DateTime.Now
                };

                Test test2 = new Test
                {
                    Name = "WorkTiredTest",
                    Category = db.TestCategory.FirstOrDefault(c => c.Name.Contains("Adult")),
                    CategoryId = db.TestCategory.FirstOrDefault(c => c.Name.Contains("Adult")).CategoryId,
                    Description = "Test mierzący poziom zmęczenia podczas pracy",
                    AddDate = DateTime.Now
                };

                try
                {
                    db.Tests.Add(test1);
                    db.Tests.Add(test2);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                db.SaveChanges();
            }

        }

    }
}
