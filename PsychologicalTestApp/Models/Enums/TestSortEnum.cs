﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.Models.Enums
{
    public enum TestSortEnum
    {
        NameAsc,
        NameDesc,
        CategoryAsc,
        CategoryDesc,
        AddDateAsc,
        AddDateDesc
    }
}
