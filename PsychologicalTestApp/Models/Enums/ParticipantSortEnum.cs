﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.Models.Enums
{
    public enum ParticipantSortEnum
    {
        FirstNameAsc,
        FirstNameDesc,
        LastNameAsc,
        LastNameDesc,
        AgeAsc,
        AgeDesc,
        GenderAsc,
        GenderDesc,
        CreateDateAsc,
        CreateDateDesc      
    }
}
