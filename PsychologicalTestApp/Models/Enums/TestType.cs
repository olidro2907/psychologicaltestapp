﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.Models.Enums
{
    public enum TestType
    {
        [Display(Name = "Dla dzieci")]
        Child,
        [Display(Name = "Dla dorosłych")]
        Adult
    }
}
