﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.Models
{
    public class TestCategory
    {
        [Required]
        [Key]
        public int CategoryId { get; set; }

        public string Name { get; set; }
    }
}
