﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.Models
{
    public class Test
    {
        [Required]
        [Key]
        public string TestId { get; set; }

        [Required]
        public string Name { get; set; }

        [ForeignKey(nameof(CategoryId))]
        public int? CategoryId { get; set; }

        public virtual TestCategory Category { get; set; }

        public string Description { get; set; }

        public bool Solved { get; set; }

        public string Result { get; set; }

        [Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime AddDate { get; internal set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime SolvedDate { get; internal set; }

        public virtual ICollection<Participant> Participants { get; set; }
    }
}
