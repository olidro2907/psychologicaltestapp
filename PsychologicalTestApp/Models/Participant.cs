﻿using PsychologicalTestApp.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PsychologicalTestApp.Models
{
    public class Participant 
    {       
        [Required]
        [Key]
        public string Id { get; set; }

        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Range(1, 116)]
        [Display(Name = "Wiek")]
        public int Age { get; set; }

        [Display(Name = "Płeć")]
        public Gender? Gender { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Numer telefonu")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Utworzony")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Zedytowany")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "Potwierdzony")]
        public bool Confirm { get; set; }

        [Required]
        [ForeignKey(nameof(UserId))]        
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey(nameof(TestId))]
        public string TestId { get; set; }
        public virtual Test Test { get; set; }
    }
}
