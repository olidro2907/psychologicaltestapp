#pragma checksum "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Account\_EmailConfirmed.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ea4ab9a83ed41961a939f1e61255097754226e06"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Account__EmailConfirmed), @"mvc.1.0.view", @"/Views/Account/_EmailConfirmed.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.Controllers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.Repositories;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ea4ab9a83ed41961a939f1e61255097754226e06", @"/Views/Account/_EmailConfirmed.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"65ddd529d6a3f6313892a490b35bb689952a129c", @"/Views/_ViewImports.cshtml")]
    public class Views_Account__EmailConfirmed : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div class=""row"">
    <div class=""col col-md-12 text-center"" id=""header""><h2><strong>Uwaga</strong></h2></div>
</div>
<div class=""row"">
    <div class=""col col-md-12 text-center"" id=""subheader"">Adres email tego konta został już potwierdzony.</div>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
