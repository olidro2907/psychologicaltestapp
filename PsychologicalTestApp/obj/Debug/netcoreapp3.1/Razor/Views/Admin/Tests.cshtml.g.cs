#pragma checksum "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3a219a07515de5de305f99910189b783aeb69831"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_Tests), @"mvc.1.0.view", @"/Views/Admin/Tests.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.Controllers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using PsychologicalTestApp.Repositories;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3a219a07515de5de305f99910189b783aeb69831", @"/Views/Admin/Tests.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"65ddd529d6a3f6313892a490b35bb689952a129c", @"/Views/_ViewImports.cshtml")]
    public class Views_Admin_Tests : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TestListVM>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/index5.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "3a219a07515de5de305f99910189b783aeb698315112", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n");
            DefineSection("navbar", async() => {
                WriteLiteral("\r\n    <section class=\"navbar\">\r\n        ");
#nullable restore
#line 8 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
   Write(Html.Partial("_Navbar_Admin"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    </section>\r\n");
            }
            );
            WriteLiteral("\r\n");
#nullable restore
#line 12 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
  
    ViewBag.Title = "Lista testów";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"container_flex\">\r\n    <div class=\"content-fluid\">\r\n        <div class=\"row\" id=\"title\">\r\n            <div class=\"col-sm-10\">\r\n                <h2>");
#nullable restore
#line 20 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
               Write(ViewBag.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2>\r\n            </div>\r\n            <div class=\"col-sm-2\" style=\"float: right;\">\r\n                <div");
            BeginWriteAttribute("class", " class=\"", 537, "\"", 545, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"float: right;\">\r\n                    <a");
            BeginWriteAttribute("href", " href=\"", 593, "\"", 625, 1);
#nullable restore
#line 24 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
WriteAttributeValue("", 600, Url.Action("CreateTest"), 600, 25, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"btn pull-right\">\r\n                        <i class=\"material-icons\" id=\"icon\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Dodaj\">&#xE147;</i>\r\n                    </a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n");
#nullable restore
#line 31 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
         if (Model.Tests.ToList().Count > 0)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"            <div class=""table-response"">
                <table class=""table table-striped table-condensed table-hover"">
                    <thead>
                        <tr>
                            <th scope=""col""><strong>#</strong></th>
                            <th scope=""col"">Nazwa</th>
                            <th scope=""col"">Kategoria</th>
                            <th scope=""col"">Użytkownicy</th>
                        </tr>
                    </thead>
                    <tbody>
");
#nullable restore
#line 44 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                          
                            var i = 0;
                            foreach (var item in Model.Tests.ToList())
                            {
                                i++;

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <tr>\r\n                                    <td>\r\n                                        ");
#nullable restore
#line 51 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                   Write(i);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                    </td>\r\n                                    <td>\r\n");
#nullable restore
#line 54 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                         if (!string.IsNullOrEmpty(item.Name.ToString()))
                                        {
                                            

#line default
#line hidden
#nullable disable
#nullable restore
#line 56 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                       Write(item.Name);

#line default
#line hidden
#nullable disable
#nullable restore
#line 56 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                                      
                                        }
                                        else
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <span class=\"text-muted\">\r\n                                                Brak\r\n                                            </span>\r\n");
#nullable restore
#line 63 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"

                                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n                                    </td>\r\n                                    <td>\r\n");
#nullable restore
#line 69 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                         if (!string.IsNullOrEmpty(@Context.TestCategory.FirstOrDefault(c => c.CategoryId == item.CategoryId).Name))
                                        {
                                            

#line default
#line hidden
#nullable disable
#nullable restore
#line 71 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                             if (@Context.TestCategory.FirstOrDefault(c => c.CategoryId == item.CategoryId).Name.Contains("Adult"))
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                <span>Dla dorosłych</span>\r\n");
#nullable restore
#line 74 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                            }
                                            else
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                <span>Dla dzieci</span>\r\n");
#nullable restore
#line 78 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                            }

#line default
#line hidden
#nullable disable
#nullable restore
#line 78 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                             
                                        }
                                        else
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <span class=\"text-muted\">\r\n                                                Brak\r\n                                            </span>\r\n");
#nullable restore
#line 85 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                    </td>\r\n                                    <td>\r\n");
#nullable restore
#line 89 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                         if (item.Participants == null)
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <span class=\"text-muted\">\r\n                                                Brak\r\n                                            </span>\r\n");
#nullable restore
#line 94 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                        }
                                        else
                                        {

                                            var participants = "";
                                            foreach (var participant in item.Participants)
                                            {
                                                participants += participant.Email + ' ';
                                            }


#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <span>");
#nullable restore
#line 104 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                             Write(participants);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n");
#nullable restore
#line 105 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n                                    </td>\r\n\r\n                                </tr>\r\n");
#nullable restore
#line 111 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"
                            }
                        

#line default
#line hidden
#nullable disable
            WriteLiteral("                    </tbody>\r\n                </table>\r\n            </div>\r\n");
#nullable restore
#line 116 "C:\Users\Asus\Documents\psychologicaltestapp\PsychologicalTestApp\Views\Admin\Tests.cshtml"

        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\r\n</div>\r\n\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public AppDbContext Context { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TestListVM> Html { get; private set; }
    }
}
#pragma warning restore 1591
