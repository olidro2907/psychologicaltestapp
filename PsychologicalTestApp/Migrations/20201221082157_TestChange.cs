﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PsychologicalTestApp.Migrations
{
    public partial class TestChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Result",
                table: "Tests",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Solved",
                table: "Tests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "SolvedDate",
                table: "Tests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Result",
                table: "Tests");

            migrationBuilder.DropColumn(
                name: "Solved",
                table: "Tests");

            migrationBuilder.DropColumn(
                name: "SolvedDate",
                table: "Tests");
        }
    }
}
