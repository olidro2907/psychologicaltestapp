﻿using Microsoft.EntityFrameworkCore;
using PsychologicalTestApp.Data;
using PsychologicalTestApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace PsychologicalTestApp.Repositories
{
    public class ParticipantRepository : IDisposable
    {
        private AppDbContext _context;

        public ParticipantRepository(AppDbContext context)
        {
            _context = context;
        }

        public DbSet<Participant> Participants { get { return _context.Participants; } }

        public DbSet<Test> Tests { get { return _context.Tests; } }

        //public DbEntityEntry<Participant> Entry(Participant entity)
        //{
        //    return _context.Entry(entity);
        //}
        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }
    }
}
